Simple html parser
============

    This is small util that allows to parse html and
     search for similar elements 


Clone repository. Execute in bash gradle gradlew fatjar

cd  build/libs
java -jar html-parsers-all-0.0.1 baseFile diffFile {optional search tag}

Please, provide full path to {baseFile} and {diffFile}

Pray! :)

Requirements
--------------------
- git
- java 8

License
=======

GNU General Public License