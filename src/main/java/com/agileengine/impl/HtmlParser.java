package com.agileengine.impl;

import com.agileengine.Constants;
import com.agileengine.IHtmlParser;
import com.agileengine.utility.JsoupCssSelect;
import com.agileengine.utility.JsoupFindById;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.joining;

/**
 * This class implements common HTML parser logic.
 */
public class HtmlParser implements IHtmlParser {


    private static Logger LOGGER = LoggerFactory.getLogger(HtmlParser.class);

    private String baseFile;
    private String diffFile;
    private String searchedElementId;


    public HtmlParser(String[] args) {
        this.baseFile = args[0];
        this.diffFile = args[1];
        if (args.length >= 3) {
            this.searchedElementId = args[2];
        } else {
            this.searchedElementId = Constants.OK_BUTTON_ELEMENT_ID;
        }
    }

    /**
     * This method implements common logic -
     * parse html and search for similar element
     *
     */
    @Override
    public void parseAndSearch() {
        try {
            Map<String, String> baseElementAttributes = parseBaseElementAttributes(baseFile, searchedElementId);
            String cssQuery = buildCssQuery(baseElementAttributes);
            Optional<Elements> foundedDiffElements = JsoupCssSelect.findElementsByQuery(new File(diffFile), cssQuery);

            if (foundedDiffElements.isPresent()) {
                compareElementsWithBaseAndScoreResult(foundedDiffElements.get(), baseElementAttributes);
            } else {
                System.out.println("=========================");
                System.out.println("No similar elements found");
                System.out.println("=========================");
            }
        } catch (Exception e) {
            LOGGER.warn("Html parser app exception", e);
        }
    }

    /**
     * This method recursively build path for founded element
     *
     * @param element
     *          Html element
     *
     * @return path to element
     *
     */
    private String buildElementsPath(Element element) {
        String elementPath = element.tagName();
        for (Element elm : element.parents()) {
            elementPath = elm.tagName() + ">" + elementPath ;
        }
        return elementPath;
    }

    private Map<String, String> parseBaseElementAttributes(String resourcePath, String targetElementId) {
        Optional<Element> element = JsoupFindById.findElementById(new File(resourcePath), targetElementId);
        return parseElementAttributes(element.get());
    }

    private Map<String, String> parseElementAttributes(Element element) {
        Map<String, String> attributes = new HashMap<>();
        element.attributes().asList()
                .forEach(attribute -> attributes.put(attribute.getKey(), attribute.getValue()));
        return attributes;
    }

    /**
     * This method build css query to search for diffs elements
     *
     * @param attributes
     *          Map of tag's atributes with values Html element
     *
     * @return path to element
     *
     */

    private String buildCssQuery(Map<String, String> attributes) {
        return attributes.entrySet()
                .stream()
                .map(attribute -> "[" + attribute.getKey() + "=" + attribute.getValue() + "]")
                .collect(joining(","));
    }

    private void compareElementsWithBaseAndScoreResult(Elements elements, Map<String, String> baseElementAttributes) {
        int maxScores = 0;
        String diffElementPath = "";
        for (Element element : elements) {
            Map<String, String> diffElementAttributes = parseElementAttributes(element);
            int elementScore = calculateElementLikelihoodScore(baseElementAttributes, diffElementAttributes);
            if (elementScore > maxScores) {
                maxScores = elementScore;
                diffElementPath = buildElementsPath(element);
            }
        }
        System.out.println("============***===========");
        System.out.println(diffElementPath);
        System.out.println("Similarity level " + (float) maxScores / baseElementAttributes.size());
        System.out.println("============***===========");
    }

    /**
     * This method calculate diffs between base and diff elements.
     *
     * @param baseElement
     *        Map element tags with values.
     *
     * @param diffElement
     *        Map element tags with values.
     *
     * @return counted similar attributes in elements.
     */
    private int calculateElementLikelihoodScore(Map<String, String> baseElement, Map<String, String> diffElement) {
        int score = 0;
        for (String key : baseElement.keySet())
            if (StringUtils.containsIgnoreCase(diffElement.get(key), baseElement.get(key))) {
                score++;
            }
        return score;
    }

}
