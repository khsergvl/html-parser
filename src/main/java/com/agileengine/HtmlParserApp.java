package com.agileengine;

import com.agileengine.impl.HtmlParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HtmlParserApp {

    private static Logger LOGGER = LoggerFactory.getLogger(HtmlParserApp.class);

    public static void main(String[] args) {

        if (args.length <2) {
            LOGGER.warn("Not enough input parameters");
            return;
        }

        try {
            HtmlParser htmlParser = new HtmlParser(args);
            htmlParser.parseAndSearch();
        } catch (Exception e) {
            LOGGER.warn("Html parser app exception", e);
        }
    }


}
