package com.agileengine;

public class Constants {
    private Constants() {

    }

    public static final String OK_BUTTON_ELEMENT_ID = "make-everything-ok-button";

    public static String CHARSET_NAME = "utf8";

}
